(defun hanks/AST-parent ()
  "Return parent node of the tree-sitter node at point."
  (interactive)
  (let ((node (tree-sitter-node-at-pos)))
    (if node
	(message "%s" (tsc-node-type (tsc-get-parent (tsc-get-parent node))))
      (message "No parent node at point"))))

(defun hanks/AST-current ()
  "Return parent node of the tree-sitter node at point."
  (interactive)
  (let ((node (tree-sitter-node-at-pos)))
    (if node
	(message "%s" (tsc-node-type (tsc-get-parent node)))
      (message "No current node at point"))))

(defun hanks/AST-named-children ()
  "Return parent node of the tree-sitter node at point."
  (interactive)
  (let ((node (tree-sitter-node-at-pos)))
    (if node
	(message "%s" (tsc-count-named-children node))
      (message "No current node at point"))))

(defun hanks/AST-next-named-sibling ()
  "Return next sibling node of the tree-sitter node at point."
  (interactive)
  (let ((node (tree-sitter-node-at-pos)))
    (if node
	(let ((next (tsc-get-next-named-sibling node)))
	  (if next
	      (message "%s" (tsc-node-type next))
	    (message "No next sibling")))
      (message "No current node at point"))))

;; Doesn't work because cursor isn't moved to the next node
(defun hanks/AST-nth-named-sibling ()
  "Return next sibling node of the tree-sitter node at point."
  (interactive)
  (let ((node (tree-sitter-node-at-pos)))
    (if node
	(let ((next (tsc-get-next-named-sibling node)))
	  (if next
	      (message "%s" (tsc-node-type next))
	    (let ((after (tsc-get-next-named-sibling node)))
	      (if after
		  (message "%s" (tsc-node-type after))
		(message "No next sibling")))
	    (message "No current node at point"))))))

(defun hanks/AST-nth-named-child ()
  "Return next sibling node of the tree-sitter node at point."
  (interactive)
  (let ((node (tree-sitter-node-at-pos)))
    (if node
	(let ((next (tsc-get-nth-named-child node 0)))
	  (if next
	      (message "%s" (tsc-node-type next))
	    (message "No next sibling")))
      (message "No current node at point"))))

;; testing adding it to the list
(defun hanks/AST-nth-named-child-test ()
  "Return next sibling node of the tree-sitter node at point."
  (interactive)
  (setq itters 0)
  (setq nodess (list))
  (let ((node (tree-sitter-node-at-pos)))
    (if node
	(let ((next (tsc-get-nth-named-child node 0)))
	  (if next
	      (push next nodess)
	    (while (< itters 2)
	      (setq itters (+ itters 1))
	      (setq after (tsc-get-nth-named-sibling node itters))
	      (if after
		  (push after nodess))
	    (message "%s" (tsc-node-type next))
	    (message "No next sibling")))
      (message "No current node at point"))))

;; Shows how you call a function from another function
(defun blahhh ()
  (interactive)
  (print "Testing"))

(defun halb ()
  (interactive)
  (blahhh)
  (setq x 1)
  (setq y 1)
  (if (eq x y)
      (print "x and y are equal")
    (print "x and y are not equal")))
(halb)

(defun testest ()
  (interactive)
  (setq x (list))
  (setq itters 0)
  (while (< itters 2)
    (setq itters (+ itters 1))
    (push itters x)
    (print itters))
  (print x))
(testest)

